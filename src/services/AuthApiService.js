import BaseApiService from "./BaseApiService";

const ENDPOINTS = {
  LOGIN: "/users/login",
};

class AuthApiService extends BaseApiService {
  login = (username, password) => 
  this.apiClient.post(ENDPOINTS.LOGIN,{username,password});
}
export default new AuthApiService();