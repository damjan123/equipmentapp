import HttpService from "../services/HttpService";

class BaseApiService {
  constructor() {
    this.http = HttpService;
    this.apiClient = HttpService.client;
  }
}
export default BaseApiService;