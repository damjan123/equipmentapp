import BaseApiService from "./BaseApiService";

const ENDPOINTS = {
  ALL_ITEMS: "/items",
  REQUESTED_ITEMS: "/itemrequests",
  ADD_ITEM: "/items",
  APPROVED_REQUEST: "itemrequests?type=1&status=3"
};
class EquipmentApiService extends BaseApiService {
  getAllItems = () => this.apiClient.get(ENDPOINTS.ALL_ITEMS);
  getAllRequestedItems = () => this.apiClient.get(ENDPOINTS.REQUESTED_ITEMS);
  postNewItem = (newItemData) => this.apiClient.post(ENDPOINTS.ADD_ITEM, newItemData);
  getAllApprovedItems = () => this.apiClient.get(ENDPOINTS.APPROVED_REQUEST)
}
export default new EquipmentApiService();