import BaseApiService from "./BaseApiService";

const ENDPOINTS = {
  ALL_USERS: "/users",
  ADD_USER: "/users",
 // PATCH_USER: "/users",
 // DELETE_USER: "users"
};

class UserApiService extends BaseApiService {
  getAllUsers = () => this.apiClient.get(ENDPOINTS.ALL_USERS);
  postNewUser = (newUserData) => this.apiClient.post(ENDPOINTS.ADD_USER, newUserData);
 // patchNewUser = (newData) => this.apiClient.patch(ENDPOINTS.PATCH_USER, newData.user_id);
  //deleteNewUser = (newData) => this.apiClient.delete(ENDPOINTS.DELETE_USER, newData.user_id);
};

export default new UserApiService();