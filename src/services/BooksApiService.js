import BaseApiService from "./BaseApiService";

const ENDPOINTS = {
  ALL_BOOKS: "/books",
  REQUESTED_BOOKS: "/bookrequests",
  POST_NEW_BOOK: "/books"

};
class BooksApiService extends BaseApiService {
  getAllBooks = () => this.apiClient.get(ENDPOINTS.ALL_BOOKS);
  getAllRequestedBooks = () => this.apiClient.get(ENDPOINTS.REQUESTED_BOOKS);
  postNewBook = (newItemData) => this.apiClient.post(ENDPOINTS.POST_NEW_BOOK,newItemData);
}


export default new BooksApiService(); 

