import * as actionTypes from '../actions/actions';

const initialState = {
    token: null,
    user: null,
    isLoading: true
}

const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SET_USER:
            return {
                ...state,
                user: action.user
            };
        case actionTypes.SET_TOKEN:
            return {
                ...state,
                token: action.token
            }
            
        case actionTypes.IS_LOADING: 
        return {
            ...state,
            isLoading: action.isLoading
        }
        default:
            return {
                ...state,
            };
    }
}




export default rootReducer;