import { React, useState, useEffect } from 'react';
import './App.css';
import Main from './containers/Main/Main';
import { useSelector } from 'react-redux';
import { Switch, Redirect, Route } from 'react-router-dom';
import Login from './components/Login/Login';
import { getAxios } from './utilts/auth';
import store from './Store/store';
import * as actionTypes from './Store/actions/actions';

function App() {
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    if (localStorage.getItem('auth-token')) {
      getAxios()(
        `http://localhost:4000/users/${localStorage.getItem('user-id')}`
      )
        .then((result) => {
          store.dispatch({
            type: actionTypes.SET_USER,
            user: result.data.result,
          });
        })
        .then((result) => setIsLoading(false))
        .catch((error) => setIsLoading(false)); /// handle error fetching user -> display message
    } else {
      setIsLoading(false);
    }
  }, []);

  const user = useSelector((state) => state.user);
  if (isLoading) {
    return 'Fetching user...';
  }
  console.log('User - App.js', user);

  return (
    <Switch>
      <Route exact path="/login" component={Login} />
      <Route path="/">{user ? <Main /> : <Redirect to="/login" />}</Route>
    </Switch>
  );
}

export default App;
