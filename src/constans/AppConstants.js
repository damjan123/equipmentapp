const ITEM_CONSTANTS = {
  STATUS : {
    FREE : 1,
    REQUESTED : 2,
    ASSIGNED : 3
  },
  ITEM_TYPES : {
    OTHER : 1,
    BOOKS : 2
  }
}

