import React from 'react';
import RegisterView from './components/Users/Users';
import Project from './components/Project/Project';
import Support from './components/Support/Support';
import ShowAllBooks from './components/Books/ShowAllBooks';
import AddNewEquipment from './components/Equipment/AddNewEquipment';
import EquipmentRequests from './components/Equipment/EquipmentRequests';
import ApprovedRequests from './components/Equipment/ApprovedRequests';
import ShowAllItems from './components/Equipment/ShowAllItems';
import Absence from './components/Absence/Absence';
import BookRequests from "./components/Books/BookRequests";

const routes = [
  {
    path: '/',
    children: [
      { path: '/register', element: <RegisterView /> },
      { path: '/project', element: <Project /> },
      { path: '/showallbooks', element: <ShowAllBooks /> },
      { path: '/bookrequests', element: <BookRequests /> },
      { path: '/support', element: <Support /> },
      { path: '/addnewitem', element: <AddNewEquipment /> },
      { path: '/equipmentrequests', element: <EquipmentRequests /> },
      { path: '/approuvedtrequests', element: <ApprovedRequests /> },
      { path: '/absence', element: <Absence /> },
      { path: '/showallitems', element: <ShowAllItems /> },
    ],
  },
];

export default routes;
