import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
//Material UI components
import CssBaseline from '@material-ui/core/CssBaseline';
import { makeStyles } from '@material-ui/core/styles';
//My Components
import NavigationBar from '../../components/NavigationBar/NavigationBar';
import SideNavigationBar from '../../components/SideNavigationBar/SideNavigationBar';
import Dashboard from '../Home/Dashboard';
import Admin from '../Admin/Admin';
import Login from '../../components/Login/Login';
import store from '../../Store/store';
import * as actionTypes from '../../Store/actions/actions';
import Users from '../../components/Users/Users';
import Project from '../../components/Project/Project';
import ShowAllBooks from '../../components/Books/ShowAllBooks';
import BookRequests from "../../components/Books/BookRequests";
import Support from '../../components/Support/Support';
import AddNewEquipment from '../../components/Equipment/AddNewEquipment';
import EquipmentRequests from '../../components/Equipment/EquipmentRequests';
import ApprovedRequests from '../../components/Equipment/ApprovedRequests';
import ShowAllItems from '../../components/Equipment/ShowAllItems';
import Absence from '../../components/Absence/Absence';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      flexShrink: 0,
    },
  },
  toolbar: theme.mixins.toolbar,
  drawerPaper: {},
  content: {
    flexGrow: 1,
    width: '90%',
    zIndex: '0',
    padding: theme.spacing(3),
  },
}));
export default Main;
function Main(props) {
  const classes = useStyles();
  const history = useHistory();
  const [isAdminOpen, setIsAdminOpen] = React.useState(false);
  const user = useSelector((state) => state.user);
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const [isOpenLoginModal, toggle] = React.useState(false);
  const [isOpenRegistrationModal, setIsOpenRegistrationModal] = React.useState(
    false
  );
  const [mobileMenu, setMobileMenu] = React.useState(null);
  const [userMenu, setUserMenu] = React.useState(null);
  const handleClickUserMenu = (event) => {
    setUserMenu(event.currentTarget);
  };
  const handleCloseUserMenu = () => {
    setUserMenu(null);
  };
  const handleOpenAdminPanel = () => {
    setIsAdminOpen(!isAdminOpen);
  };
  const logOut = () => {
    store.dispatch({
      type: actionTypes.SET_USER,
      user: null,
    });

    localStorage.setItem('auth-token', '');
    localStorage.removeItem('user-id');
    history.push('/login');
  };
  const handleClick = (event) => {
    setMobileMenu(event.currentTarget);
  };
  const handleClose = () => {
    setMobileMenu(null);
  };
  function handlOpenModal(open) {
    toggle(open);
  }
  function handlOpenRegistrationModal(open) {
    setIsOpenRegistrationModal(open);
  }
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  console.log('User object', user);
  return (
    <div className={classes.root}>
      <CssBaseline />
      <NavigationBar
        userMenu={userMenu}
        setUserMenu={setUserMenu}
        user={user}
        logOut={logOut}
        handlOpenModal={handlOpenModal}
        handleClickUserMenu={handleClickUserMenu}
        handleCloseUserMenu={handleCloseUserMenu}
        isOpenRegistrationModal={isOpenRegistrationModal}
        handlOpenRegistrationModal={handlOpenRegistrationModal}
        isOpen={isOpenLoginModal}
        handleDrawerToggle={handleDrawerToggle}
        mobileMenu={mobileMenu}
        handleClick={handleClick}
        handleClose={handleClose}
      />
      <SideNavigationBar
        mobileOpen={mobileOpen}
        handleOpenAdminPanel={handleOpenAdminPanel}
        isAdminOpen={isAdminOpen}
        setMobileOpen={setMobileOpen}
        handleDrawerToggle={handleDrawerToggle}
      />
      <main className={classes.content}>
        <div className={classes.toolbar} />
        <Switch>
          <Route path="/home" component={Dashboard} exact />
          <Route path="/admin" component={Admin} exact />
          <Route path="/support" component={Support} exact />
          <Route path="/users" component={Users} exact />
          <Route path="/project" component={Project} exact />
          <Route path="/showallbooks" component={ShowAllBooks} exact />
          <Route path="/bookrequests" component={BookRequests} exact />
          <Route path="/addnewitem" component={AddNewEquipment} exact />
          <Route path="/approvedrequests" component={ApprovedRequests} exact />
          <Route path="/showallitems" component={ShowAllItems} exact />
          <Route path="/absence" component={Absence} exact />
          <Route
            path="/equipmentrequests"
            component={EquipmentRequests}
            exact
          />
          <Route path="/login" component={Login} exact />
          <Route exact path="/">
            <Redirect to="/home" />
          </Route>
        </Switch>
      </main>
    </div>
  );
}
