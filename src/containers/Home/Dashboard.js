import React,{ useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import  { formatDateTime } from "../../components/Users/Users"
import UserApiService from "../../services/UserApiService";
import EquipmentApiService from '../../services/EquipmentApiService';
import BooksApiService from "../../services/BooksApiService";
import {
  Card,
  CardContent,
  Grid,
  Typography,
  colors,
  makeStyles,
} from '@material-ui/core';


const useStyles = makeStyles((theme) => ({
  root: {
    display: 'inline-flex',
    height: '40%',
    width: '20%',
    margin: '20px',
  },
  avatar: {
    backgroundColor: colors.red[600],
    height: 56,
    width: 56,
  },
  differenceIcon: {
    color: colors.red[900],
  },
  footer:  {
  position: "fixed",
  left: 0,
  bottom: 0,
  width: "100%",
  color: "gray",
  textAlign: "center",
},
  differenceValue: {
    color: colors.red[900],
    marginRight: theme.spacing(1),
  },
}));

const Dashboard = ({ className, staticContext, ...rest }) => {
  const classes = useStyles();
  
  const [AllUsers, setAllUsers] = useState([]);

  const fetchAllUsers = async () => {
    let result = await UserApiService.getAllUsers().then((res) => res.data.result);
    result = result.map((user) => {
      user.createdAt = formatDateTime(user.createdAt);
      user.updatedAt = formatDateTime(user.updatedAt);
      return user;
    });
    setAllUsers(result);
  };

  React.useEffect(() => {
    fetchAllUsers();
  }, []);

  const [AllItems, setAllItems] = useState([]);
  
  const fetchAllItems = async () => {
    let result = await EquipmentApiService.getAllItems()
    setAllItems(result.data.result);
  };
  React.useEffect(() => {
    fetchAllItems();
  }, []);

  const [AllBooks, setAllBooks] = useState([]);
  const fetchAllBooks = async () => {
    let result = await BooksApiService.getAllBooks()
    setAllBooks(result.data.result);
  };
  React.useEffect(() => {
    fetchAllBooks();
  }, []);
  const[EquipmentRequested, setEquipmentRequests] = useState([])
  const fetchEquipmentRequests = async () => {
    let result = await EquipmentApiService.getAllRequestedItems()
    .then((res) => res.data.result);
    result = result.map((el) => {
      return {
        request_id: el.request_id,
        full_name: el.User.full_name,
        comment: el.comment,
        bar_code: el.bar_code,
        item_name: el.Item.name,
        status: el.Item.status,
        dateandtime: el.date_time,
      }
    })
    setEquipmentRequests(result);
  };
  React.useEffect(() => {
    fetchEquipmentRequests();
  }, []);
  return (
    <React.Fragment>
      <Card className={clsx(classes.root, className)} {...rest}>
        <CardContent>
          <Grid container justify="space-between" spacing={3}>
            <Grid item>
              <Typography color="textSecondary" gutterBottom variant="h6">
                Number of registred users
              </Typography>
              <Typography color="textPrimary" variant="h3">
              {AllUsers.length}
              </Typography>
            </Grid>
          </Grid>
        </CardContent>
      </Card>

      <Card className={clsx(classes.root, className)} {...rest}>
        <CardContent>
          <Grid container justify="space-between" spacing={3}>
            <Grid item>
              <Typography color="textSecondary" gutterBottom variant="h6">
                Number of Items
              </Typography>
              <Typography color="textPrimary" variant="h3">
                {AllItems.length}
              </Typography>
            </Grid>
          </Grid>
        </CardContent>
      </Card>

      <Card className={clsx(classes.root, className)} {...rest}>
        <CardContent>
          <Grid container justify="space-between" spacing={3}>
            <Grid item>
              <Typography color="textSecondary" gutterBottom variant="h6">
                Number of free Items
              </Typography>
              <Typography color="textPrimary" variant="h3">
                16
              </Typography>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
      <Card className={clsx(classes.root, className)} {...rest}>
        <CardContent>
          <Grid container justify="space-between" spacing={3}>
            <Grid item>
              <Typography color="textSecondary" gutterBottom variant="h6">
                Number of Assigned Items
              </Typography>
              <Typography color="textPrimary" variant="h3">
                34
              </Typography>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
      <Card className={clsx(classes.root, className)} {...rest}>
        <CardContent>
          <Grid container justify="space-between" spacing={3}>
            <Grid item>
              <Typography color="textSecondary" gutterBottom variant="h6">
                Number of Requested Items
              </Typography>
              <Typography color="textPrimary" variant="h3">
                {EquipmentRequested.length}
              </Typography>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
      <Card className={clsx(classes.root, className)} {...rest}>
        <CardContent>
          <Grid container justify="space-between" spacing={3}>
            <Grid item>
              <Typography color="textSecondary" gutterBottom variant="h6">
                Number of Books
              </Typography>
              <Typography color="textPrimary" variant="h3">
              {AllBooks.length}
              </Typography>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
      <Card className={clsx(classes.root, className)} {...rest}>
        <CardContent>
          <Grid container justify="space-between" spacing={3}>
            <Grid item>
              <Typography color="textSecondary" gutterBottom variant="h6">
                Number of Requested Books
              </Typography>
              <Typography color="textPrimary" variant="h3">
                5
              </Typography>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
      <Card className={clsx(classes.root, className)} {...rest}>
        <CardContent>
          <Grid container justify="space-between" spacing={3}>
            <Grid item>
              <Typography color="textSecondary" gutterBottom variant="h6">
                Number of Assigned Books
              </Typography>
              <Typography color="textPrimary" variant="h3">
                5
              </Typography>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
      <Card className={clsx(classes.root, className)} {...rest}>
        <CardContent>
          <Grid container justify="space-between" spacing={3}>
            <Grid item>
              <Typography color="textSecondary" gutterBottom variant="h6">
                Number of Free Books
              </Typography>
              <Typography color="textPrimary" variant="h3">
                20
              </Typography>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
      <div className={classes.footer}>
    <p>Copyright© UnderIT 2021 </p>
  </div>
    </React.Fragment>
    
  );
};
Dashboard.propTypes = {
  className: PropTypes.string,
};
export default Dashboard;
