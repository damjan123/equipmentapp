import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { BrowserRouter } from 'react-router-dom';
import App from './App';
import { Provider } from 'react-redux';
import store from './Store/store';
import setAutherizationToken from './utilts/auth';
import * as actionTypes from './Store/actions/actions';
import jwt from 'jsonwebtoken';

if (localStorage.jwtToken) {
  setAutherizationToken(localStorage.jwtToken);
  store.dispatch({
    type: actionTypes.SET_USER,
    user: jwt.decode(localStorage.jwtToken).user,
  });
}
ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,

  document.getElementById('root')
);
