import React from 'react';
import MaterialTable from 'material-table';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import EditIcon from '@material-ui/icons/Edit';
import { tableIcons } from '../../constans/table-constans';
import "../Users/Users.css"

export default function Project() {
  const [open, setOpen] = React.useState(false);
  const handleClickOpen = () => {
    setOpen(true);
  };
  const { useState } = React;
  const handleClose = () => {
    setOpen(false);
  };
  const edit = {
    icon: () => <EditIcon />,
    tooltip: 'edit',
    onClick: () => {},
  };
  const [columns] = useState([
    { title: 'ID', field: 'id' },
    { title: 'PROJECT NAME', field: 'name' },
    { title: 'CONTACT EMAIL', field: 'email' },
    { title: 'DATE FROM', field: 'datefrom' },
    { title: 'DATE TO', field: 'dateto' },
    { title: 'TYPE', field: 'type' },
    { title: 'MESSAGE', field: 'message' },
    { title: 'STATUS', field: 'status' },
    { title: 'ACTION', field: 'action' },
  ]);
  const [data] = useState([
    {
      id: '#1234',
      datefrom: '02.2021',
      dateto: '07.2022',
      name: 'NinaMedia',
      email: 'blabla@gmail.com',
      status: 'in progress',
    },
    {
      id: '#6545',
      datefrom: '05.2021',
      dateto: '09.2021',
      name: 'UndeIt',
      email: 'damjanadzic@gmail.com',
      status: 'done',
    },
    {
      id: '#7566',
      datefrom: '04.2021',
      dateto: '10.2021',
      name: 'Instana',
      email: 'adzicdamjan@yahoo.com',
      status: 'done',
    },
    {
      id: '#7891',
      datefrom: '01.2021',
      dateto: '08.2021',
      name: 'NimmaHub',
      email: '123damjan@gmail.com',
      status: 'in pogress',
    },
  ]);

  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <Button
        style={{
          alignSelf: 'flex-start',
          backgroundColor: '#3295DA',
          color: 'white',
        }}
        variant="outlined"
        onClick={handleClickOpen}
      >
        Add new Project
      </Button>
      <br></br>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle
          style={{ backgroundColor: '#3295DA', color: 'white' }}
          id="form-dialog-title"
        >
          Add New Project
        </DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="id"
            label="ID"
            type="name"
            fullWidth
          />
          <TextField
            margin="dense"
            id="fullname"
            label="Project Name"
            type="name"
            fullWidth
          />
          <TextField
            margin="dense"
            id="email"
            label="Email"
            type="email"
            fullWidth
          />
          <TextField margin="dense" id="datefrom" type="date" fullWidth />
          <TextField margin="dense" id="dateto" type="date" fullWidth />
          <TextField
            margin="dense"
            id="type"
            label="Type"
            type="text"
            fullWidth
          />
          <TextField
            margin="dense"
            id="message"
            label="Message"
            type="text"
            fullWidth
          />
          <TextField
            margin="dense"
            id="status"
            label="Status"
            type="text"
            fullWidth
          />
          <TextField
            margin="dense"
            id="action"
            label="Action"
            type="text"
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button
            onClick={handleClose}
            style={{
              alignSelf: 'flex-start',
              backgroundColor: '#3295DA',
              color: 'white',
            }}
          >
            Cancel
          </Button>
          <Button
            onClick={handleClose}
            style={{
              alignSelf: 'flex-start',
              backgroundColor: '#3295DA',
              color: 'white',
            }}
          >
            Add
          </Button>
        </DialogActions>
      </Dialog>
      <div style={{ maxWidth: '100%' }}>
        <MaterialTable
          options={{
            headerStyle: { backgroundColor: '#3295DA', color: 'white' },
          }}
          actions={[edit]}
          icons={tableIcons}
          columns={columns}
          data={data}
          title="Project List"
        />
      </div>
      <div className="footer">
    <p>Copyright© UnderIT 2021</p>
  </div>
    </div>
  );
}
