import React from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

export default function SimpleMenu(props) {

  return (
    <div>
      <Button aria-controls="simple-menu" aria-haspopup="true" onClick={props.handleClickUserMenu}>
        <span style={{color:'white'}}>{props.text}</span>
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={props.userMenu}
        keepMounted
        open={Boolean(props.userMenu)}
        onClose={props.handleCloseUserMenu}
      >
        <MenuItem onClick={props.handleCloseUserMenu}>Profile</MenuItem>
        <MenuItem onClick={props.handleCloseUserMenu}>My account</MenuItem>
        <MenuItem onClick={() =>{props.handleCloseUserMenu(); props.logOut();}}>Logout</MenuItem>
      </Menu>
    </div>
  );
}