import React,{ useState } from 'react';
import MaterialTable from 'material-table';
import { tableIcons } from '../../constans/table-constans';
import BookApiService from "../../services/BooksApiService";
import "../Users/Users.css"

export default function BookRequests() {
  const [AllBooks, setAllBooks] = useState([]);

  const [columns] = useState([
    { title: 'REQUEST ID', field: 'id' },
    { title: 'NAME', field: 'name' },
    { title: 'COMMENT', field: 'comment' },
    { title: 'BAR CODE', field: 'bar_code' },
    { title: 'BOOK TITLE', field: 'booktitle' },
    { title: 'STATUS', field: 'status',lookup: { 1: 'Free',  2: "Requested", 3: 'Assigned',} },
    { title: 'DATE AND TIME', field: 'dateandtime' },
    { title: 'ACTION', field: 'action' },
  ]);
  const fetchAllBooks = async () => {
    let result = await BookApiService.getAllRequestedBooks()
    .then((res) => res.data.result);
    result = result.map((el) => {
      return {
        id: el.request_id,
        name: el.User.full_name,
        comment: el.comment,
        bar_code: el.Item.bar_code,
        booktitle: el.Item.name,
        status: el.Item.status,
        dateandtime: el.date_time,
      }
    })
    setAllBooks(result);
  };
  React.useEffect(() => {
    fetchAllBooks();
  }, []);
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <div style={{ maxWidth: '100%' }}>
        <MaterialTable
          options={{
            headerStyle: { backgroundColor: '#3295DA', color: 'white' },
          }}
          icons={tableIcons}
          title="Book Requests"
          columns={columns}
          data={AllBooks}
        />
      </div>
      <div className="footer">
    <p>Copyright© UnderIT 2021</p>
  </div>
    </div>
  );
}
