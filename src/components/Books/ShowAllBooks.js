import React from 'react';
import MaterialTable from 'material-table';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { tableIcons } from '../../constans/table-constans';
import { useSelector } from 'react-redux';
import { useForm } from 'react-hook-form';
import BooksApiService from "../../services/BooksApiService";
import "../Users/Users.css"

export default function ShowAllBooks() {
  const [open, setOpen] = React.useState(false);
  const user = useSelector((state) => state.user);
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const { useState } = React;
  const [AllBooks, setAllBooks] = useState([]);
  const {register, handleSubmit,setError } = useForm();

  const [columns] = useState([
    { title: 'NAME', field: 'name' },
    { title: 'AUTHOR', field: 'author' },
    { title: 'BAR CODE', field: 'bar_code' },
    { title: 'DESCRIPTION', field: 'description' },
    { title: 'STATUS', field: 'status',lookup: { 1: 'Free', 3: 'Assigned', 2: "Requested" } },
    { title: 'ACTION', field: 'action' },
  ]);
  const fetchAllBooks = async () => {
    let result = await BooksApiService.getAllBooks()
    setAllBooks(result.data.result);
  };
  React.useEffect(() => {
    fetchAllBooks();
  }, []);

  const handleAddNewBook = async (newItemData) => {
    await BooksApiService.postNewBook(newItemData)
      .then((newItem) => {
        let items = AllBooks;
        items.push(newItem);
        setAllBooks(items);
      })
      .then( () => { 
        handleClose() 
        fetchAllBooks()
      })
      .catch((err) => {
        if (err?.response?.data) {
          setError('name', {
            type: 'required',
            message: err.response.data,
          });
        } else {
        }
        console.log('ERROR:\n ' + err);
        console.dir(err);
      });  
  };
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      {user.user_type === 1 && (
        <Button
          style={{
            alignSelf: 'flex-start',
            backgroundColor: '#3295DA',
            color: 'white',
          }}
          variant="outlined"
          onClick={handleClickOpen}
        >
          Add new Book
        </Button>
      )}
      <br></br>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle
          style={{ backgroundColor: '#3295DA', color: 'white' }}
          id="form-dialog-title"
        >
          Add New Book
        </DialogTitle>
        <form onSubmit={handleSubmit(handleAddNewBook)}>
        <DialogContent>
          <TextField
            autoFocus
            inputRef={register({
              required: true,
            })}
            margin="dense"
            id="bar_code"
            name="bar_code"
            inputProps={{ maxLength: 9 }}
            label="Bar Code"
            type="text"
            fullWidth
          />
          <TextField
          inputRef={register({
            required: true,
          })}
            margin="dense"
            name="name"
            id="name"
            label="Name"
            type="text"
            fullWidth
          />
          <TextField
          inputRef={register({
            required: true,
          })}
            margin="dense"
            id="author"
            name="author"
            label="Author"
            type="text"
            fullWidth
          />
          <TextField
          inputRef={register({
            required: true,
          })}
            margin="dense"
            name="description"
            id="description"
            label="Description"
            type="text"
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button
            onClick={handleClose}
            style={{
              alignSelf: 'flex-start',
              backgroundColor: '#3295DA',
              color: 'white',
            }}
          >
            Cancel
          </Button>
          <Button
            onClick={ () => {
              document.getElementById('btn').click()
            }}
            style={{
              alignSelf: 'flex-start',
              backgroundColor: '#3295DA',
              color: 'white',
            }}
          >
            Add
          </Button>
          <input hidden id="btn" type="submit" />
        </DialogActions>
        </form>
      </Dialog>

      <div style={{ maxWidth: '100%' }}>
        <MaterialTable
          options={{
            headerStyle: { backgroundColor: '#3295DA', color: 'white' },
            pageSize: 10,
          }}
          icons={tableIcons}
          title="Book List"
          columns={ columns }
          data={AllBooks}
        />
      </div>
      <div className="footer">
    <p>Copyright© UnderIT 2021</p>
  </div>
    </div>
  );
}
