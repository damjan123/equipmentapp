import React from 'react';
import MaterialTable from 'material-table';
import DeleteIcon from '@material-ui/icons/Delete';
import { tableIcons } from '../../constans/table-constans';
import "../Users/Users.css"

export default function Users() {
  const edit = {
    icon: () => <DeleteIcon />,
    tooltip: 'Delete Item',
    onClick: (event, rowData) => {},
  };
  const { useState } = React;
  const [columns] = useState([
    { title: 'Name', field: 'name' },
    { title: 'Bar Code', field: 'barcode' },
    { title: 'Comment', field: 'comment' },
    { title: 'Status', field: 'status' },
  ]);
  const [data] = useState([
    {
      name: 'Logitech tastatura',
      barcode: '123654258951',
      status: 'not working',
    },
    {
      name: 'Philips Monitor 24',
      barcode: '258951123654',
      status: 'not working',
    },
    {
      name: 'Dell Monitor 22',
      barcode: '895112365425',
      status: 'not working',
    },
  ]);
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <div style={{ maxWidth: '100%' }}>
        <MaterialTable
          options={{
            headerStyle: { backgroundColor: '#3295DA', color: 'white' },
          }}
          actions={[edit]}
          icons={tableIcons}
          columns={columns}
          data={data}
          title="Broken Items"
        />
      </div>
      <div className="footer">
    <p>Copyright© UnderIT 2021</p>
  </div>
    </div>
  );
}
