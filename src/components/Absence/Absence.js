import React from 'react';
import MaterialTable from 'material-table';
import EditIcon from '@material-ui/icons/Edit';
import { tableIcons } from '../../constans/table-constans';
import { useSelector } from 'react-redux';
import DialogContent from '@material-ui/core/DialogContent';
import TextField from '@material-ui/core/TextField';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import "../Users/Users.css";

export default function Absence() {
  const { useState } = React;
  const edit = {
    icon: () => <EditIcon />,
    tooltip: 'edit',
    onClick: (event, rowData) => {},
  };
  const [columns] = useState([
    { title: 'ID', field: 'id' },
    { title: 'FULL NAME', field: 'name' },
    { title: 'EMAIL', field: 'email' },
    { title: 'DATE FROM', field: 'datefrom' },
    { title: 'DATE TO', field: 'dateto' },
    { title: 'TYPE', field: 'type' },
    { title: 'MESSAGE', field: 'message' },
    { title: 'STATUS', field: 'status' },
    { title: 'ACTION', field: 'action' },
  ]);
  const [data] = useState([
    {
      id: '#1234',
      datefrom: '13.02.2021',
      dateto: '20.02.2021',
      name: 'Damjan Adzic',
      email: 'damjana.adzic@gmail.com',
    },
    {
      id: '#6545',
      datefrom: '02.05.2021',
      dateto: '10.05.2021',
      name: 'Strahinja Drinic',
      email: 'strahinja@gmail.com',
    },
    {
      id: '#7566',
      datefrom: '22.04.2021',
      dateto: '28.04.2021',
      name: 'Predrag Sarac',
      email: 'pedja@yahoo.com',
    },
  ]);
  const user = useSelector((state) => state.user);
  const typeOfTimeOff = [
    {
      value: 'sick',
      label: 'Sick',
    },
    {
      value: 'Vacation',
      label: 'Vacation',
    },
    {
      value: 'Time off without pay',
      label: 'Time off without pay',
    },
    {
      value: 'Military',
      label: 'Military',
    },
    {
      value: 'Jury Duty',
      label: 'Jury Duty',
    },
    {
      value: 'Maternity/Paternity',
      label: 'Maternity/Paternity',
    },
    {
      value: 'Other',
      label: 'Other',
    },
  ];
  const [absence, setCurrency] = React.useState('EUR');
  const handleChange = (event) => {
    setCurrency(event.target.value);
  };

  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <div style={{ maxWidth: '100%' }}>
        {user.user_type === 1 ? (
          <MaterialTable
            options={{
              headerStyle: { backgroundColor: '#3295DA', color: 'white' },
            }}
            actions={[edit]}
            icons={tableIcons}
            columns={columns}
            data={data}
            title="Absence Request"
          />
        ) : null}
      </div>
      {user.user_type === 0 ? (
        <div
          style={{
            display: 'inline-block',
            position: 'fixed',
            top: '10%',
            left: '35%',
            width: '40%',
            padding: '20px 20px',
            border: ' 2px solid #C1C1C1 ',
            borderRadius: '10px',
            marginTop: '20px',
            fontweight: '400',
          }}
        >
          <DialogContent>
            <DialogTitle
              style={{
                alignSelf: 'flex-start',
                backgroundColor: '#3295DA',
                color: 'white',
                borderRadius: '5px',
                marginBottom: '25px',
              }}
            >
              Absence Request
            </DialogTitle>
            <TextField margin="dense" id="datefrom" type="date" fullWidth />
            <TextField margin="dense" id="dateto" type="date" fullWidth />

            <TextField
              id="type-of-time-off"
              margin="dense"
              select
              label="Type of Time off"
              value={absence}
              fullWidth
              onChange={handleChange}
            >
              {typeOfTimeOff.map((option) => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </TextField>
            <TextField
              multiline
              margin="dense"
              id="message"
              label="Message"
              type="text"
              rows={4}
              variant="outlined"
              fullWidth
            />
          </DialogContent>

          <Button
            style={{
              marginTop: '10px',
              marginLeft: '24px',
              alignSelf: 'flex-start',
              backgroundColor: '#3295DA',
              color: 'white',
            }}
          >
            Submit
          </Button>
        </div>
      ) : null}
      <div className="footer">
    <p>Copyright© UnderIT 2021</p>
  </div>
    </div>
    
  );
}
