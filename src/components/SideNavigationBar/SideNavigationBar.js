import React, { useState } from 'react';
import { Link} from 'react-router-dom';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Collapse from '@material-ui/core/Collapse';
import { useSelector } from 'react-redux';
import Hidden from '@material-ui/core/Hidden';
import BuildIcon from '@material-ui/icons/Build';
import HeadsetMicIcon from '@material-ui/icons/HeadsetMic';
import DirectionsRunIcon from '@material-ui/icons/DirectionsRun';
import { useHistory } from 'react-router-dom';
import AccountTreeIcon from '@material-ui/icons/AccountTree';
import PersonIcon from '@material-ui/icons/Person';
import ImportContactsIcon from '@material-ui/icons/ImportContacts';
import DashboardIcon from '@material-ui/icons/Dashboard';
import QueryBuilderIcon from '@material-ui/icons/QueryBuilder';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import { makeStyles, useTheme } from '@material-ui/core/styles';


const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    zIndex: '0',
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
      zIndex: '0',
    },
  },
  links: {
    textDecoration: 'none',
    color: 'white',
  },
  toolbar: theme.mixins.toolbar,

  drawerPaper: {
    width: drawerWidth,
    backgroundColor: '#1D1D1B',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },

  nested: {
    paddingLeft: theme.spacing(4),
    color: 'white',
  },
}));
const drawerWidth = 240;
const SideNavigationBar = (props) => {
  const history = useHistory();
  const { window } = props;
  const classes = useStyles();
  const theme = useTheme();
  const user = useSelector((state) => state.user);
  const [listItemSelected, setListItemSelected] = useState(-1);
  const drawer = (
    <div>
      <div className={classes.toolbar} />
      <Divider />
      <List>
        {user.user_type === 1 && (
          <Link to="/home" className={classes.links}>
            <ListItem button onClick={() => props.setMobileOpen(false)}
            >
              <ListItemIcon>
                <DashboardIcon style={{ color: 'white' }} />
              </ListItemIcon>
              <ListItemText primary={'Dashboard'} />
            </ListItem>
          </Link>
        )}

        {user.user_type === 1 && (
          <Link to="/users" className={classes.links}>
            <ListItem  button onClick={() => props.setMobileOpen(false)}>
              <ListItemIcon>
                <PersonIcon style={{ color: 'white' }} />
              </ListItemIcon>
              <ListItemText primary="User" />
            </ListItem>
          </Link>
        )}

        
        <ListItem
          className={classes.links}
          button
          onClick={() => {
            if (listItemSelected === 1) {
              setListItemSelected(-1);
            } else {
              setListItemSelected(1);
            }
          }}
        >
          <ListItemIcon>
            <BuildIcon style={{ color: 'white' }} />
          </ListItemIcon>
          <ListItemText primary="Equipment" />
          {listItemSelected === 1 ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={listItemSelected === 1} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            {user.user_type === 1 && (
              <Link to="/addnewitem" className={classes.links}>
                <ListItem className={classes.nested} button>
                  <ListItemText primary="Add New Item" />
                </ListItem>
              </Link>
            )}
            {user.user_type === 1 && (
            <Link to="/equipmentrequests" className={classes.links}>
              <ListItem className={classes.nested} button>
                <ListItemText primary="Equipment Requests" />
              </ListItem>
            </Link>
            )}
            {user.user_type === 1 && (
            <Link to="/approvedrequests" className={classes.links}>
              <ListItem className={classes.nested} button>
                <ListItemText primary="Approved Requests" />
              </ListItem>
            </Link>
            )}
            <Link to="/showallitems" className={classes.links}>
              <ListItem className={classes.nested} button>
                <ListItemText primary="Show All Items" />
              </ListItem>
            </Link>
          </List>
        </Collapse>

        {user.user_type === 1 && (
          <Link to="/project" className={classes.links}>
            <ListItem  button onClick={() => props.setMobileOpen(false)}>
              <ListItemIcon>
                <AccountTreeIcon style={{ color: 'white' }} />
              </ListItemIcon>
              <ListItemText primary="Project" />
            </ListItem>
          </Link>
        )}

        <ListItem
          className={classes.links}
          button
          onClick={() => {
            if (listItemSelected === 3) {
              setListItemSelected(-3);
            } else {
              setListItemSelected(3);
            }
          }}
        >
          <ListItemIcon>
            <QueryBuilderIcon style={{ color: 'white' }} />
          </ListItemIcon>
          <ListItemText primary="Time Tracking" />
          {listItemSelected === 3 ? <ExpandLess /> : <ExpandMore />}
        </ListItem>

        <Collapse in={listItemSelected === 3} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <Link to="/" className={classes.links}>
              <ListItem className={classes.nested} button>
                <ListItemText primary="Test" />
              </ListItem>
            </Link>
          </List>
        </Collapse>

        <ListItem
          className={classes.links}
          button
          onClick={() => {
            if (listItemSelected === 2) {
              setListItemSelected(-2);
            } else {
              setListItemSelected(2);
            }
          }}
        >
          <ListItemIcon>
              <ImportContactsIcon style={{ color: 'white' }} />
            </ListItemIcon>
          <ListItemText primary="Books" />
          {listItemSelected === 2 ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={listItemSelected === 2} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            {user.user_type === 1 && (
            <Link to="/bookrequests" className={classes.links}>
              <ListItem className={classes.nested} button>
                <ListItemText primary="Book Requests" />
              </ListItem>
            </Link>
            )}
            
            <Link to="/showallbooks" className={classes.links}>
              <ListItem className={classes.nested} button>
                <ListItemText primary="Show All Books" />
              </ListItem>
            </Link>
          </List>
        </Collapse>

        <ListItem
          className={classes.links}
          button
          onClick={() => {
            if (listItemSelected === 5) {
              setListItemSelected(-1);
            } else {
              setListItemSelected(5);
            }
          }}
        >
          <ListItemIcon>
            <DirectionsRunIcon style={{ color: 'white' }} />
          </ListItemIcon>
          <ListItemText primary="Absence" />
          {listItemSelected === 5 ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={listItemSelected === 5} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <Link to="/absence" className={classes.links}>
              <ListItem className={classes.nested} button>
                <ListItemText primary="Absence Request" />
              </ListItem>
            </Link>
          </List>
        </Collapse>
        {user.user_type === 1 && (
          <Link to="/support" className={classes.links}>
            <ListItem  button onClick={() => props.setMobileOpen(false)}>
              <ListItemIcon>
                <HeadsetMicIcon style={{ color: 'white' }} />
              </ListItemIcon>
              <ListItemText primary="Support" />
            </ListItem>
          </Link>
        )}
      </List>
    </div>
  );
  const container =
    window !== undefined ? () => window().document.body : undefined;
  return history.location.pathname !== '/login' ? (
    <nav className={classes.drawer} aria-label="mailbox folders">
      {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
      <Hidden smUp implementation="css">
        <Drawer
          container={container}
          variant="temporary"
          anchor={theme.direction === 'rtl' ? 'right' : 'left'}
          open={props.mobileOpen}
          onClose={props.handleDrawerToggle}
          classes={{
            paper: classes.drawerPaper,
          }}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
        >
          {drawer}
        </Drawer>
      </Hidden>
      <Hidden xsDown implementation="css">
        <Drawer
          classes={{
            paper: classes.drawerPaper,
          }}
          variant="permanent"
        >
          {drawer}
        </Drawer>
      </Hidden>
    </nav>
  ) : (
    ''
  );
};
export default SideNavigationBar;
