import React, { useState }from 'react';
import MaterialTable from 'material-table';
import { tableIcons } from '../../constans/table-constans';
import axios from '../../utilts/axios';
import EquipmentApiService from '../../services/EquipmentApiService';
import "../Users/Users.css";


export default function ShowAllItems() {

  const [columns] = useState([
    { title: 'BAR CODE', field: 'bar_code' },
    { title: 'NAME', field: 'name' },
    { title: 'STATUS', field: 'status',lookup: { 1: 'Free', 2: "Requested", 3: "Assigned"}, },
    { title: 'DESCRIPTION', field: 'description' },
  ]);
  const [AllItems, setAllItems] = useState([]);

  const fetchAllItems = async () => {
    let result = await EquipmentApiService.getAllItems()
    setAllItems(result.data.result);
  };
  React.useEffect(() => {
    fetchAllItems();
  }, []);

  const itemswithFreeStats= AllItems.filter((el) => el.status===1)
  const updateItemStatus = async (barcode) => {
    await axios.patch(`items/${barcode}`, {
      status:2
    })
  }
  React.useEffect(() => {
    fetchAllItems();
  }, []);

  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <div style={{ maxWidth: '100%' }}>
        <MaterialTable
          options={{
            headerStyle: { backgroundColor: '#3295DA', color: 'white' },
            pageSize: 10,
            actionsColumnIndex: -1
          }}
          actions={[
            {
              icon: 'save',
              tooltip: 'request',
              onClick: (event, rowData) => {
                updateItemStatus(rowData.bar_code)
                fetchAllItems();
              }
            },
            
        ]}
          icons={tableIcons}
          columns={columns}
          data={itemswithFreeStats}
          title="All items"
        />
      </div>
      <div className="footer">
    <p>Copyright© UnderIT 2021</p>
  </div>
    </div>
  );
}
