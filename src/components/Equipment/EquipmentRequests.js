import React, { useState } from 'react';
import MaterialTable from 'material-table';
import { tableIcons } from '../../constans/table-constans';
import EquipmentApiService from  "../../services/EquipmentApiService";
import axios from '../../utilts/axios';
import "../Users/Users.css";

export default function EquipmentRequests() {
  const[EquipmentRequested, setEquipmentRequests] = useState([])
  const [columns] = useState([
    { title: 'REQUEST ID', field: 'request_id' },
    { title: 'NAME', field: 'full_name' },
    { title: 'COMMENT', field: 'comment' },
    { title: 'BAR CODE', field: 'bar_code' },
    { title: 'ITEM NAME', field: 'item_name' },
    { title: 'STATUS', field: 'status',lookup: { 1: 'Free', 2: "Requested", 3: 'Assigned' } },
    { title: 'DATE AND TIME', field: 'dateandtime' },
  ]);
  
  const fetchEquipmentRequests = async () => {
    let result = await axios.get(`items?type=1&status=2`)
    console.log(result)
    // .then((res) => res.data.result);
    // result = result.map((el) => {
    //   return {
    //     request_id: el.request_id,
    //     full_name: el.User.full_name,
    //     comment: el.comment,
    //     bar_code: el.bar_code,
    //     item_name: el.Item.name,
    //     status: el.Item.status,
    //     dateandtime: el.date_time,
    //   }
    // })
    setEquipmentRequests(result);
  };
  React.useEffect(() => {
    fetchEquipmentRequests();
  }, []);

  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <div style={{ maxWidth: '100%' }}>
        <MaterialTable
          options={{
            headerStyle: { backgroundColor: '#3295DA', color: 'white' },
            actionsColumnIndex: -1
          }}
          icons={tableIcons}
          columns={columns}
          data={EquipmentRequested}
          title="Equipment Requests"
         
          actions={[
            {
              icon: 'save',
              tooltip: 'Accept',
              onClick: (event, rowData) => {

              }
            },
            rowData => ({
              icon: 'delete',
              tooltip: 'Decline',
              onClick: (event, rowData) => {
  
              },
              disabled: rowData.birthYear < 2000
            })
        ]}
        />
      </div>
      <div className="footer">
    <p>Copyright© UnderIT 2021</p>
  </div>
    </div>
  );
}
