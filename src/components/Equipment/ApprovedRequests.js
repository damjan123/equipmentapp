import React,{ useState } from 'react';
import MaterialTable from 'material-table';
import { tableIcons } from '../../constans/table-constans';
import EquipmentApiService from  "../../services/EquipmentApiService";
import "../Users/Users.css"
export default function ApprovedRequests() {

  const[EquipmentApproved, setEquipmentApproved] = useState([])
  const [columns] = useState([
    { title: 'NUMBER OF REQUEST', field: 'request' },
    { title: 'NAME', field: 'name' },
    { title: 'EMAIL', field: 'email' },
    { title: 'BAR CODE', field: 'barcode' },
    { title: 'STATUS', field: 'status',lookup: { 1: 'Free',  2: "Requested", 3: 'Assigned' } },
  ]);
  const fetchEquipmentApproved = async () => {
    let result = await EquipmentApiService.getAllApprovedItems()
    
    .then((res) => res.data.result);
    console.log(result);
    result = result.map((el) => {
      return {
        request: el.request_id,
        name: el.full_name,
        email: el.email,
        bar_code: el.bar_code,
        status: el.status,
      }
    })
    setEquipmentApproved(result);
  };
  React.useEffect(() => {
    fetchEquipmentApproved();
  }, []);
  
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <div style={{ maxWidth: '100%' }}>
        <MaterialTable
          options={{
            headerStyle: { backgroundColor: '#3295DA', color: 'white' },
          }}
          icons={tableIcons}
          columns={columns}
          data={EquipmentApproved}
          title="Approved Requests"
        />
      </div>
      <div className="footer">
    <p>Copyright© UnderIT 2021</p>
  </div>
    </div>
  );
}
