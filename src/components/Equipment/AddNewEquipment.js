import React, {useState} from 'react';
import TextField from '@material-ui/core/TextField';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import DialogContent from '@material-ui/core/DialogContent';
import { useForm } from 'react-hook-form';
import EquipmentApiService from "../../services/EquipmentApiService";
import "../Users/Users.css"

export default function AddNewEquipment() {
  
  const [ AllItems, setAllItems] = useState([]);
  const { register, handleSubmit, setError } = useForm();
  const onSubmit = (data, e) => {
    e.target.reset();
  };
  const handleAddNewItem = async (newItemData) => {
    await EquipmentApiService.postNewItem(newItemData)
      .then((newItem) => {
        let items = AllItems;
        items.push(newItem);
        setAllItems(items);
      })
      .catch((err) => {
        if (err?.response?.data) {
          setError('name', {
            type: 'required',
            message: err.response.data,
          });
        } else {
        }
        console.log('ERROR:\n ' + err);
        console.dir(err);
      });
  };
  
  return (
    <form onSubmit={handleSubmit(handleAddNewItem,onSubmit)}>
      <div
        style={{
          display: 'inline-block',
          position: 'fixed',
          top: '10%',
          left: '35%',
          width: '40%',
          padding: '20px 20px',
          border: ' 2px solid #C1C1C1 ',
          borderRadius: '10px',
          marginTop: '20px',
          fontweight: '400',
        }}
      >
        <DialogContent>
          <DialogTitle
            style={{
              alignSelf: 'flex-start',
              backgroundColor: '#3295DA',
              color: 'white',
              borderRadius: '5px',
              marginBottom: '25px',
            }}
          >
            ADD EQUIPMENT
          </DialogTitle>
          <TextField
          required
          inputRef={register({
            required: true,
          })}
            autoFocus
            name = "bar_code"
            margin="dense"
            id="bar_code"
            inputProps={{ maxLength: 9 }}
            label="Bar Code"
            type="text"
            fullWidth
          />
          <TextField
          required
          inputRef={register({
            required: true,
          })}
            margin="dense"
            name = "name"
            id="name"
            inputProps={{ maxLength: 44 }}
            label="Name"
            type="name"
            fullWidth
          />
          <TextField
            multiline
            inputRef={register({
              required: true,
            })}
            name = "description"
            margin="dense"
            id="description"
            inputProps={{ maxLength: 45 }}
            label="Description"
            type="text"
            fullWidth
          />
        </DialogContent>
        <Button
          onClick={() => {
            document.getElementById('btn').click();
          }}
          style={{
            marginTop: '10px',
            marginLeft: '24px',
            alignSelf: 'flex-start',
            backgroundColor: '#3295DA',
            color: 'white',
          }}
          type="reset"
        >
          Add equipment
        </Button>
        <input hidden id="btn" type="submit" />
      </div>
      <div className="footer">
    <p>Copyright© UnderIT 2021</p>
  </div>
    </form>
  );
}
