import React, { useState } from 'react';
import MaterialTable from 'material-table';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import axios from '../../utilts/axios.js';
import { useForm } from 'react-hook-form';
import moment from 'moment';
import { tableIcons } from '../../constans/table-constans';
import UserApiService from "../../services/UserApiService";
import "./Users.css";



export const formatDateTime = (val) => {
  return moment(val).format('DD. MMM YYYY HH:mm:ss ');
};

export default function Users() {
  const [open, setOpen] = React.useState(false);
  const [AllUsers, setAllUsers] = useState([]);
  const { register, handleSubmit, errors, setError } = useForm();
  const handleClickOpen = () => {
    setOpen(true);
  };
  const emailValidation = (email) => {
    const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(email);
  };
  const [columns] = useState([
    {
      title: 'FULL NAME',
      field: 'full_name',
      validate: (rowData) =>
        rowData.full_name === ''
          ? { isValid: false, helperText: 'Field cannot be empty' }
          : true,
    },
    {
      title: 'EMAIL',
      field: 'username',
      validate: (rowData) => {
        if (emailValidation(rowData.username)) return true;
        else return 'Incorrect email format';
      },
    },
    {
      title: 'USER TYPE',
      field: 'user_type',
      lookup: { 1: 'Admin', 0: 'User' },
    },
    { title: 'CREATED AT', field: 'createdAt', editable: 'never' },
    { title: 'UPDATED AT', field: 'updatedAt', editable: 'never' },
    {
      title: 'ONLINE STATUS',
      field: 'online_status',
      lookup: { 1: 'Online', 0: 'Offline' },
    },
  ]);
  const fetchAllUsers = async () => {
    let result = await UserApiService.getAllUsers().then((res) => res.data.result);
    result = result.map((user) => {
      user.createdAt = formatDateTime(user.createdAt);
      user.updatedAt = formatDateTime(user.updatedAt);
      return user;
    });
    setAllUsers(result);
  };
  React.useEffect(() => {
    fetchAllUsers();
  }, []);


  const handleAddNewRow = async (newUserData) => {
    newUserData.user_type = 0;

    await UserApiService.postNewUser(newUserData)
      .then((newUser) => {
        let users = AllUsers;
        users.push(newUser);
        setAllUsers(users);
        handleClose();
      })
      .catch((err) => {
        if (err?.response?.data) {
          setError('username', {
            type: 'required',
            message: err.response.data,
          });
          console.log('Same username error', err.response.data);
        } else {
        }
        console.log('ERROR:\n ' + err);
        console.dir(err);
      });
    fetchAllUsers();
  };

  const updateUser = async (newData, oldData) => {
    await axios.patch(`users/${newData.user_id}`, {
      username: newData.username,
      full_name: newData.full_name,
      password: newData.password,
      user_type: newData.user_type,
      online_status: newData.online_status,
    });
    fetchAllUsers();
  };

  const deleteUser = async (newData, oldData) => {
    await axios.delete(`users/${newData.user_id}`, {});
    fetchAllUsers();
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <Button
        style={{
          alignSelf: 'flex-start',
          backgroundColor: '#3295DA',
          color: 'white',
          padding: '10px',
        }}
        variant="outlined"
        onClick={handleClickOpen}
      >
        Add new user
      </Button>
      <br></br>

      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle
          style={{ backgroundColor: '#3295DA', color: 'white' }}
          id="form-dialog-title"
        >
          Add New User
        </DialogTitle>
        <form onSubmit={handleSubmit(handleAddNewRow)}>
          <DialogContent>
            <TextField
              multiline
              inputRef={register({
                required: true,
                pattern: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i,
              })}
              autoFocus
              name="username"
              margin="dense"
              id="username"
              label="Email Address"
              type="email"
              fullWidth
            />
            {errors.username && (
              <span style={{ color: 'red' }}>
                {errors.username?.message
                  ? '*' + errors.username.message
                  : '*Incorrect email format'}
              </span>
            )}
            <TextField
              required
              inputRef={register({
                required: true,
              })}
              margin="dense"
              id="full_name"
              inputProps={{ maxLength: 44 }}
              label="Full Name"
              type="name"
              name="full_name"
              fullWidth
            />
            {errors.full_name && (
              <span style={{ color: 'red' }}>*This field is required</span>
            )}

            <TextField
              required
              inputRef={register({
                required: true,
                pattern: /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/,
              })}
              margin="dense"
              id="password"
              name="password"
              label="Password"
              type="password"
              fullWidth
            />
            {errors.password && (
              <span style={{ color: 'red' }}>
                *Password must have at least one number, at least one special
                character and min 6 characters
              </span>
            )}
          </DialogContent>
          <DialogActions>
            <Button
              onClick={handleClose}
              style={{
                alignSelf: 'flex-start',
                backgroundColor: '#3295DA',
                color: 'white',
              }}
            >
              Cancel
            </Button>
            <Button
              onClick={() => {
                document.getElementById('btn').click();
              }}
              style={{
                alignSelf: 'flex-start',
                backgroundColor: '#3295DA',
                color: 'white',
              }}
            >
              Add
            </Button>
            <input hidden id="btn" type="submit" />
          </DialogActions>
        </form>
      </Dialog>

      <div style={{ maxWidth: '100%' }}>
        <MaterialTable
          title="Users List"
          options={{
            headerStyle: { backgroundColor: '#3295DA', color: 'white' },
            pageSize: 10,
          }}
          editable={{
            onRowUpdate: (newData, oldData) => updateUser(newData, oldData),
            onRowDelete: (newData, oldData) => deleteUser(newData, oldData),
            onRowUpdateCancelled: (rowData) =>
              console.log('Row editing cancelled'),
          }}
          icons={tableIcons}
          columns={columns}
          data={AllUsers}
        />
        
      </div>    
      <div className="footer">
    <p>Copyright© UnderIT 2021</p>
  </div>
    </div>
  );
}
