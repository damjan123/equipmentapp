import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import UserMenu from './userMenu';
import mainLogo from '../../assets/underitlogo.png';
import { useSelector } from 'react-redux';
import './userMenu.css';

export default function NavigationBar(props) {
  const user = useSelector((state) => state.user);
  return (
    <AppBar position="fixed" className="appBar">
      <Toolbar>
        <a href="/home">
          <img className="logo" alt="" src={mainLogo} />
        </a>
        {
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={props.handleDrawerToggle}
            className="menuButton"
          >
            <MenuIcon />
          </IconButton>
        }
        {
          <div
            style={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'flex-end',
              width: '100%',
            }}
          >
            <h2 style={{ marginRight: '10px' }}>{user?.full_name}</h2>
            <UserMenu
              userMenu={props.userMenu}
              logOut={props.logOut}
              setUserMenu={props.setUserMenu}
              handleClickUserMenu={props.handleClickUserMenu}
              handleCloseUserMenu={props.handleCloseUserMenu}
            />
          </div>
        }
      </Toolbar>
    </AppBar>
  );
}
