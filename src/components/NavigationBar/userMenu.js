import React from 'react';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Badge from '@material-ui/core/Badge';
import NotificationsIcon from '@material-ui/icons/Notifications';
import PersonIcon from '@material-ui/icons/Person';
import PermIdentityTwoToneIcon from '@material-ui/icons/PermIdentityTwoTone';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import Dialog from '@material-ui/core/Dialog';
import IconButton from '@material-ui/core/IconButton';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Typography from '@material-ui/core/Typography';

export default function SimpleMenu(props) {
  const [open, setOpen] = React.useState(false);
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  return (
    <div>
      <IconButton color="inherit" onClick={handleClickOpen}>
        <Badge
          badgeContent={9}
          aria-controls="simple-menu"
          aria-haspopup="true"
        >
          <NotificationsIcon
            style={{
              color: 'white',
              fontSize: '30px',
              display: 'inline-block',
            }}
          />
        </Badge>
      </IconButton>

      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          Notifications
        </DialogTitle>
        <DialogContent dividers>
          <Typography gutterBottom>
            Cras mattis consectetur purus sit amet fermentum. Cras justo odio,
            dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta
            ac consectetur ac, vestibulum at eros. Aenean lacinia bibendum nulla
            sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl
            consectetur et. Donec sed odio dui. Donec ullamcorper nulla non
            metus auctor fringilla.
          </Typography>
        </DialogContent>
      </Dialog>

      <IconButton
        aria-controls="simple-menu"
        aria-haspopup="true"
        onClick={props.handleClickUserMenu}
      >
        <PermIdentityTwoToneIcon style={{ color: 'white', fontSize: '30px' }} />
      </IconButton>
      <Menu
        id="simple-menu"
        anchorEl={props.userMenu}
        keepMounted
        open={Boolean(props.userMenu)}
        onClose={props.handleCloseUserMenu}
      >
        <MenuItem onClick={props.handleCloseUserMenu}>
          <ExitToAppIcon style={{ color: '#1D1D1B' }} />
          Change Password
        </MenuItem>
        <MenuItem
          onClick={() => {
            props.handleCloseUserMenu();
            props.logOut();
          }}
        >
          <PersonIcon style={{ color: '#1D1D1B' }} />
          Logout
        </MenuItem>
      </Menu>
    </div>
  );
}
