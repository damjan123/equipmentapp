import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import { useHistory } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import store from '../../Store/store';
import * as actionTypes from '../../Store/actions/actions';
import setAutherizationToken from '../../utilts/auth';
import AuthApiService from "../../services/AuthApiService";
import "../Users/Users.css"

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: '#1D1D1B',
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor: '#3295DA',
  },
}));
export default function Login(props) {
  const classes = useStyles();
  const { register, handleSubmit } = useForm();
  const history = useHistory();
  const submitHandler = async (data) => {
  const loginRes = await AuthApiService.login(data.email , data.password);

    store.dispatch({
      type: actionTypes.SET_USER,
      user: loginRes.data.data.user,
    });

    setAutherizationToken(loginRes.data.token);
    localStorage.setItem('auth-token', loginRes.data.token);
    localStorage.setItem('user-id', loginRes.data.data.user.user_id);
    history.push('/home');
  };
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Login
        </Typography>
        <form
          className={classes.form}
          onSubmit={handleSubmit(submitHandler)}
          noValidate
        >
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            inputRef={register}
            label="Email"
            name="email"
            autoComplete="email"
            autoFocus
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            inputRef={register}
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            LOGIN
          </Button>
          <Grid container>
            <Grid item>
              <Link href="#" variant="body2">
                Forgot password ?
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <div className="footer">
    <p>Copyright© UnderIT 2021</p>
  </div>
    </Container>
  );
}
